package id.co.allianz.microservice.ecommerce.lib.kafka;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableConfigurationProperties
@ConfigurationProperties(prefix = "azlife.lib.kafka")
public class KafkaConfig {
    private String bootstrapServers;
    private String groupId;
    private Integer maxRetry;
    private Long initialInterval;
    private Long maxInterval;
    private Double multiplier;

    public String getBootstrapServers() {
        return bootstrapServers;
    }

    public void setBootstrapServers(String bootstrapServers) {
        this.bootstrapServers = bootstrapServers;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public Integer getMaxRetry() {
        if (maxRetry == null)
            maxRetry = 1;

        return maxRetry;
    }

    public void setMaxRetry(int maxRetry) {
        this.maxRetry = maxRetry;
    }

    public Long getMaxInterval() {

        if (maxInterval == null)
            maxInterval = 30000l;

        return maxInterval;
    }

    public void setMaxInterval(Long maxInterval) {

        this.maxInterval = maxInterval;
    }

    public Long getInitialInterval() {
        if (initialInterval == null || initialInterval.longValue() < 100)
            initialInterval = 100l;

        return initialInterval;
    }

    public void setInitialInterval(Long initialInterval) {
        this.initialInterval = initialInterval;
    }

    public Double getMultiplier() {

        if (multiplier == null)
            multiplier = 2.0;

        return multiplier;
    }

    public void setMultiplier(Double multiplier) {
        this.multiplier = multiplier;
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public boolean equals(Object obj) {
        return EqualsBuilder.reflectionEquals(this, obj);
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.JSON_STYLE);
    }
}
