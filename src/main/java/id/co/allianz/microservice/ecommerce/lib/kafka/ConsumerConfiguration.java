package id.co.allianz.microservice.ecommerce.lib.kafka;

import java.util.HashMap;
import java.util.Map;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.listener.DeadLetterPublishingRecoverer;
import org.springframework.kafka.listener.SeekToCurrentErrorHandler;
import org.springframework.retry.backoff.ExponentialBackOffPolicy;
import org.springframework.retry.support.RetryTemplate;

@Configuration
public class ConsumerConfiguration {
    private static final Logger LOGGER = LoggerFactory.getLogger(ConsumerConfiguration.class);

    private KafkaConfig kafkaConfig;

    public ConsumerConfiguration(KafkaConfig kafkaConfig) {
        this.kafkaConfig = kafkaConfig;
        LOGGER.debug("ConsumerConfiguration: {}", this.kafkaConfig);
    }

    @Bean
    public ConsumerFactory<Object, Object> consumerFactory() {
        return new DefaultKafkaConsumerFactory<>(consumerConfigurations());
    }

    @Bean
    public Map<String, Object> consumerConfigurations() {
        Map<String, Object> configurations = new HashMap<>();

        configurations.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, kafkaConfig.getBootstrapServers());
        configurations.put(ConsumerConfig.GROUP_ID_CONFIG, kafkaConfig.getGroupId());
        configurations.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        configurations.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);

        return configurations;
    }

    @Bean
    ConcurrentKafkaListenerContainerFactory<String, String> kafkaListenerContainerFactory(
            KafkaTemplate<Object, Object> template) {
        ConcurrentKafkaListenerContainerFactory<String, String> factory = new ConcurrentKafkaListenerContainerFactory<>();
        factory.setConsumerFactory(consumerFactory());
        factory.setRetryTemplate(retryTemplate());
        factory.setErrorHandler(
                new SeekToCurrentErrorHandler(new DeadLetterPublishingRecoverer(template), kafkaConfig.getMaxRetry()));
        return factory;
    }

    @Bean
    public RetryTemplate retryTemplate() {
        ExponentialBackOffPolicy backOffPolicy = new ExponentialBackOffPolicy();
        backOffPolicy.setInitialInterval(kafkaConfig.getInitialInterval());
        backOffPolicy.setMaxInterval(kafkaConfig.getMaxInterval());
        backOffPolicy.setMultiplier(kafkaConfig.getMultiplier());
        RetryTemplate template = new RetryTemplate();
        template.setBackOffPolicy(backOffPolicy);
        return template;
    }
}
